<?php get_header(); ?>

<?php get_template_part('partials/breadcrumbs'); ?>


    <section class="">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <?php
                $currentlang = pll_current_language();
                if ($currentlang == "ru"):
                    ?>
                    <h2 class="title" style="margin-bottom:0">	Новости
                    </h2>
                <?php elseif ($currentlang == "en"): ?>
                    <h2 class="title" style="margin-bottom:0">News
                    </h2>
                <?php else: ?>
                    <h2 class="title" style="margin-bottom:0">	Жаңалықтар
                    </h2>
                <?php endif;
                ?>

                                             </div>
        </div>
    </div>
    </section>

    <section class="">
        <div class="container">
            <?php
            $currentlang = pll_current_language();
            if ($currentlang == "ru"):
                ?>
<!--                <div class="row">-->
<!--                    <div class="col-md-12">-->
<!--                        <ul class="news-menu" role="tablist">-->
<!--                            <li class="news-menu__item active">-->
<!--                                <a data-toggle="tab" href="#tabs-1" role="tab">Инвестиции</a>-->
<!--                            </li>-->
<!--                            <li class="news-menu__item">-->
<!--                                <a data-toggle="tab" href="#tabs-2" role="tab">Мероприятия</a>-->
<!--                            </li>-->
<!--                            <li class="news-menu__item">-->
<!--                                <a data-toggle="tab" href="#tabs-3" role="tab">Аналитика</a>-->
<!--                            </li>-->
<!--                            <li class="news-menu__item">-->
<!--                                <a data-toggle="tab" href="#tabs-4" role="tab">Отчеты</a>-->
<!--                            </li>-->
<!--                            <li class="news-menu__item">-->
<!--                                <a data-toggle="tab" href="#tabs-5" role="tab">Разное</a>-->
<!--                            </li>-->
<!--                        </ul>-->
<!--                    </div>-->
<!--                </div>-->
            <?php elseif ($currentlang == "en"): ?>

            <?php else: ?>

            <?php endif;
            ?>
            <div class="row">
                <div class="col-md-12">
                    <div class="news-content">
                        <?php
                        $i = 0;
                        if (have_posts()) : ?>
                            <?php
                            //the_archive_title('<h1 class="page-header">', '</h1>');
                            //the_archive_description('<div class="taxonomy-description">', '</div>');
                            ?>
                            <?php
                            while (have_posts()) : the_post();
                                get_template_part('partials/blog', 'teaser');
                                $i++;
                                // 3 items per row
                                if ($i % 3 == 0) echo "";
                            endwhile;
                        else :
                            get_template_part('partials/content', 'post');
                        endif;
                        ?>

                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12" style="text-align: center">
                    <?php
                    get_template_part('partials/blog', 'pager');
                    ?>
                </div>
            </div>
        </div>
    </section>
<?php get_footer(); ?>