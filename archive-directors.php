<?php get_header(); ?>
<?php get_template_part('partials/breadcrumbs'); ?>

    <section class="portfolio">
        <div class="container">
            <div class="row">
                <div class="col-md-12">

                    <?php
                    $currentlang = pll_current_language();
                    if ($currentlang == "ru"):
                        ?>
                        <h4 class="upper-title">
                            Совет директоров
                        </h4>
                        <h2 class="title" style="margin-bottom:0">
                            СОСТАВ СОВЕТА ДИРЕКТОРОВ АО «НГК «ТАУ-КЕН САМРУК»
                        </h2>
                    <?php elseif ($currentlang == "en"): ?>
                        <h4 class="upper-title">
                            Bords of Directors
                        </h4>
                        <h2 class="title" style="margin-bottom:0">
                            Bords of Directors
                        </h2>
                    <?php else: ?>
                        <h4 class="upper-title">
                            Директорлар кеңесі
                        </h4>
                        <h2 class="title" style="margin-bottom:0">
                            Директорлар кеңесі
                        </h2>
                    <?php endif;
                    //                echo $currentlang;
                    ?>


                </div>
            </div>
        </div>
    </section>

    <section class="">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="news-content">
                        <?php
                        $i = 0;
                        if (have_posts()) : ?>
                            <?php
                            //the_archive_title('<h1 class="page-header">', '</h1>');
                            //the_archive_description('<div class="taxonomy-description">', '</div>');
                            ?>
                            <?php
                            while (have_posts()) : the_post();
                                get_template_part('partials/blog', 'directors');
                                $i++;
                                // 3 items per row
                                if ($i % 2 == 0) echo "<div class=clearfix></div><br>";
                            endwhile;
                        else :
                            get_template_part('partials/content', 'post');
                        endif;
                        ?>

                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12" style="text-align: center">
                    <?php
                    get_template_part('partials/blog', 'pager');
                    ?>
                </div>
            </div>
        </div>
    </section>
<?php get_footer(); ?>